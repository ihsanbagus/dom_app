package id.co.pt8bit.dom.helpers;

import java.text.NumberFormat;
import java.util.Currency;

public class NumberHelper {

    public static String currency(int angka) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setMaximumFractionDigits(0);
        format.setCurrency(Currency.getInstance("IDR"));
        return format.format(angka);
    }

    public static String indoFormat(String num) {
        if (num.substring(0, 2).equals("62")) {
            return "0" + num.substring(2);
        }
        return num;
    }
}
