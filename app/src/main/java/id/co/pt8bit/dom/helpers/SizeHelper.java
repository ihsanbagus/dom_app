package id.co.pt8bit.dom.helpers;

import android.content.Context;

public class SizeHelper {

    public static int intToDP(Context ctx, int number) {
        float scale = ctx.getResources().getDisplayMetrics().density;
        return (int) (number * scale + 0.5f);
    }
}
