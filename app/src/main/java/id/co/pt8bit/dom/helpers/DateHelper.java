package id.co.pt8bit.dom.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {

    public static String dateNow(String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern, Locale.getDefault());//DateFormat.getDateInstance();//new SimpleDateFormat(pattern);
        Date dateobj = new Date();
        return df.format(dateobj);
    }

    public static String[] getDateTime() {
        Calendar c = Calendar.getInstance();
        String[] dateTime = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        return dateTime;
    }
}
