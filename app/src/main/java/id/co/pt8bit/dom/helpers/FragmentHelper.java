package id.co.pt8bit.dom.helpers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.co.pt8bit.dom.R;
import id.co.pt8bit.dom.fragments.AccountFragment;
import id.co.pt8bit.dom.fragments.DashboardFragment;
import id.co.pt8bit.dom.models.MainMenuModel;

public class FragmentHelper {

    public static List<MainMenuModel> menuHomeList() {
        List<MainMenuModel> menus = new ArrayList<>();
        menus.add(new MainMenuModel(R.drawable.ic_dashboard, R.string.dashboard));
        menus.add(new MainMenuModel(R.drawable.ic_account, R.string.account));
        menus.add(new MainMenuModel(R.drawable.ic_message, R.string.message));
        menus.add(new MainMenuModel(R.drawable.ic_attendance, R.string.attendance));
        menus.add(new MainMenuModel(R.drawable.ic_settings, R.string.setting));
        return menus;
    }

    public static void showFragmentHome(AppCompatActivity act, int position) {
        List<Fragment> fragmentHomeList = new ArrayList<>();
//        fragmentHomeList.add(DashboardFragment.newInstance(menuHomeList().get(0)));
//        fragmentHomeList.add(AccountFragment.newInstance(menuHomeList().get(0)));
//        fragmentHomeList.add(SettingFragment.newInstance(screenTitles[POS_ACCOUNT], ""));
//        fragmentHomeList.add(AccountFragment.newInstance(screenTitles[POS_ATTENDANCE], ""));
//        fragmentHomeList.add(null);
//        fragmentHomeList.add(AccountFragment.newInstance(screenTitles[POS_SETTINGS], ""));
        act.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentHomeList.get(position))
                .commit();
    }
}
