package id.co.pt8bit.dom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.pt8bit.dom.R;
import id.co.pt8bit.dom.materials.CustomButton;

public class MainActivity extends BaseActivity {
    int flag = 0;
    @BindView(R.id.lav_actionBar)
    LottieAnimationView lavActionBar;
    @BindView(R.id.my_btn)
    CustomButton myBtn;
    @BindView(R.id.lav_thumbUp)
    LottieAnimationView lavThumbUp;
    @BindView(R.id.lav_thumbDown)
    LottieAnimationView lavThumbDown;
    @BindView(R.id.lav_toggle)
    LottieAnimationView lavToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        lavThumbUp = findViewById(R.id.lav_thumbUp);
        lavThumbUp.setOnClickListener(view -> {
            lavThumbDown.setProgress(0);
            lavThumbDown.pauseAnimation();
            lavThumbUp.playAnimation();
            Toast.makeText(MainActivity.this, "Cheers!!", Toast.LENGTH_SHORT).show();
            //---- Your code here------
        });

        lavThumbDown = findViewById(R.id.lav_thumbDown);
        lavThumbDown.setOnClickListener(view -> {
            lavThumbUp.setProgress(0);
            lavThumbUp.pauseAnimation();
            lavThumbDown.playAnimation();
            Toast.makeText(MainActivity.this, "Boo!!", Toast.LENGTH_SHORT).show();
            //---- Your code here------
        });

        lavToggle = findViewById(R.id.lav_toggle);
        lavToggle.setOnClickListener(view -> changeState());
        myBtn.setOnClickListener(view -> startActivity(new Intent(this, HomeActivity.class)));
    }

    private void changeState() {
        if (flag == 0) {
            lavToggle.setMinAndMaxProgress(0f, 0.43f); //Here, calculation is done on the basis of start and stop frame divided by the total number of frames
            lavToggle.playAnimation();
            flag = 1;
            //---- Your code here------
        } else {
            lavToggle.setMinAndMaxProgress(0.5f, 1f);
            lavToggle.playAnimation();
            flag = 0;
            //---- Your code here------
        }
    }
}