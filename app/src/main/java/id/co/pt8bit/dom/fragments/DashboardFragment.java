package id.co.pt8bit.dom.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.pt8bit.dom.R;
import id.co.pt8bit.dom.adapters.SliderAdapterExample;
import id.co.pt8bit.dom.models.SliderItem;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment {

    @BindView(R.id.imageSlider)
    SliderView imageSlider;
    private Context ctx;
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam2;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(Context ctx, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        fragment.ctx = ctx;
        Bundle args = new Bundle();
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, v);
        slider();
        return v;
    }

    private void slider() {
        SliderAdapterExample sa = new SliderAdapterExample(ctx);
        imageSlider.setSliderAdapter(sa);
        imageSlider.setIndicatorEnabled(true);
        imageSlider.setIndicatorAnimation(IndicatorAnimationType.SCALE); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.POPTRANSFORMATION);
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        imageSlider.setIndicatorSelectedColor(Color.WHITE);
        imageSlider.setIndicatorUnselectedColor(Color.GRAY);
        imageSlider.setScrollTimeInSec(4); //set scroll delay in seconds :
        imageSlider.startAutoCycle();
        SliderItem si;
        si = new SliderItem("Title 1", "https://c0.wallpaperflare.com/preview/477/561/102/business-office-meetings-marketing.jpg");
        sa.addItem(si);
        si = new SliderItem("Title 2", "https://c0.wallpaperflare.com/preview/956/909/290/business-office-computer-flatlay.jpg");
        sa.addItem(si);
        si = new SliderItem("Title 3", "https://c0.wallpaperflare.com/preview/811/935/645/adults-analysis-brainstorming-collaboration.jpg");
        sa.addItem(si);
        si = new SliderItem("Title 4", "https://c0.wallpaperflare.com/preview/758/797/254/achievement-adult-business-business-people.jpg");
        sa.addItem(si);
        si = new SliderItem("Title 3", "https://c0.wallpaperflare.com/preview/911/937/224/accomplishment-achievement-adult-bangkok.jpg");
        sa.addItem(si);
    }

    @OnClick({R.id.attendance, R.id.payroll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.attendance:
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, AttendanceFragment.newInstance("", ""))
                        .commitAllowingStateLoss();
                break;
            case R.id.payroll:
                break;
        }
    }
}