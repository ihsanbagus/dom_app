package id.co.pt8bit.dom.activities;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.pt8bit.dom.R;
import id.co.pt8bit.dom.fragments.AccountFragment;
import id.co.pt8bit.dom.fragments.AttendanceFragment;
import id.co.pt8bit.dom.fragments.DashboardFragment;
import id.co.pt8bit.dom.fragments.MessageFragment;
import id.co.pt8bit.dom.fragments.SettingFragment;
import id.co.pt8bit.dom.menus.DrawerAdapter;
import id.co.pt8bit.dom.menus.DrawerItem;
import id.co.pt8bit.dom.menus.SimpleItem;
import id.co.pt8bit.dom.menus.SpaceItem;

public class HomeActivity extends BaseActivity implements DrawerAdapter.OnItemSelectedListener {
    private static final int POS_DASHBOARD = 0;
    private static final int POS_ACCOUNT = 1;
    private static final int POS_MESSAGES = 2;
    private static final int POS_ATTENDANCE = 3;
    private static final int POS_SETTINGS = 5;
    private static final int POS_LOGOUT = 6;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    FrameLayout container;

    private List<Fragment> fragmentList = new ArrayList<>();
    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setToolbar(toolbar, getResources().getString(R.string.dashboard), null, null, null, null);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        fragmentList.add(DashboardFragment.newInstance(this, screenTitles[POS_DASHBOARD]));
        fragmentList.add(AccountFragment.newInstance(screenTitles[POS_ACCOUNT], ""));
        fragmentList.add(MessageFragment.newInstance(screenTitles[POS_MESSAGES], ""));
        fragmentList.add(AttendanceFragment.newInstance(screenTitles[POS_ATTENDANCE], ""));
        fragmentList.add(null);
        fragmentList.add(SettingFragment.newInstance(screenTitles[POS_SETTINGS], ""));

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_DASHBOARD).setChecked(true),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_MESSAGES),
                createItemFor(POS_ATTENDANCE),
                new SpaceItem(48),
                createItemFor(POS_SETTINGS),
                createItemFor(POS_LOGOUT)));
        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_DASHBOARD);
    }

    @Override
    public void onItemSelected(int position) {
        if (position == POS_LOGOUT) {
            finish();
        }
        slidingRootNav.closeMenu();
        showFragment(fragmentList.get(position));
        toolbar.setTitle(screenTitles[position]);
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @SuppressWarnings("rawtypes")
    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
//                .withIconTint(color(R.color.textColorSecondary))
                .withTextTint(color(R.color.textColorPrimary))
//                .withSelectedIconTint(color(R.color.colorAccent))
                .withSelectedTextTint(color(R.color.colorAccent));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    @Override
    public void onBackPressed() {
        if (slidingRootNav.isMenuOpened()) {
            slidingRootNav.closeMenu(true);
        } else {
            super.onBackPressed();
        }
    }
}