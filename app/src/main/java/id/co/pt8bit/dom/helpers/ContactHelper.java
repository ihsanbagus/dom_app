package id.co.pt8bit.dom.helpers;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import id.co.pt8bit.dom.R;

public class ContactHelper {

    public static void getOne(AppCompatEditText v, Uri contactData) {
        Context ctx = v.getContext();
        Cursor c = ctx.getContentResolver().query(contactData, null, null, null, null);
        if (c.moveToFirst()) {
            String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor numbers = ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
            int i = 0;
            String name = "";
            String[] phoneNum = new String[numbers.getCount()];
            String[] type = new String[numbers.getCount()];
            while (numbers.moveToNext()) {
                name = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                phoneNum[i] = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                type[i] = (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(ctx.getResources(), numbers.getInt(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)), ""); // insert a type string in front of the number
                i++;
            }
            makeDialogForMultipleNumbers(v, phoneNum, type, name);
        }
        c.close();
    }

    private static void makeDialogForMultipleNumbers(AppCompatEditText v, String[] phoneNum, String[] type, String name) {
        Context ctx = v.getContext();
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_multiple_numbers);
        RadioGroup r = dialog.findViewById(R.id.multipleNumberGroup);
        TextView displayName = dialog.findViewById(R.id.displayName);
        displayName.setText(name);
        WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();
        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
        lWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lWindowParams);

        for (int k = 0; k < phoneNum.length; k++) {
            RadioButton radioButton = new RadioButton(ctx);
            radioButton.setText(type[k] + "\n" + phoneNum[k]);
            radioButton.setPadding(0, 15, 0, 15);
            radioButton.setId(k);
            radioButton.setTag(phoneNum[k].replaceAll("[^0-9]", ""));
            r.addView(radioButton);
        }
        dialog.show();

        r.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton checkedRadio = group.findViewById(group.getCheckedRadioButtonId());
            String num = checkedRadio.getTag().toString();
            if (num.startsWith("62")) {
                num = "0" + num.substring(2);
            }
            v.setText(num);
            dialog.dismiss();
        });
    }
}
