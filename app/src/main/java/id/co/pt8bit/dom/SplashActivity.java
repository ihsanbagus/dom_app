package id.co.pt8bit.dom;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.widget.AppCompatTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.pt8bit.dom.activities.BaseActivity;
import id.co.pt8bit.dom.activities.HomeActivity;
import id.co.pt8bit.dom.activities.WizardActivity;
import id.co.pt8bit.dom.helpers.AnimationHelper;
import id.co.pt8bit.dom.helpers.CookieHelper;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.pt_name)
    AppCompatTextView ptName;
    @BindView(R.id.app_name)
    AppCompatTextView appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        AnimationHelper.fadeIn(ptName, 3);
        new Handler().postDelayed(() -> {
            int wizard = CookieHelper.restoreI(SplashActivity.this, "wizard");
            if (wizard == 0) {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, WizardActivity.class));
            }
            finish();
        }, 3000);
    }
}