package id.co.pt8bit.dom.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class CookieHelper {
    private static String database = "settings";

    public static void store(Context ctx, String key, String val) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(database, MODE_PRIVATE);
        sharedPreferences.edit().putString(key, val).apply();
    }

    public static void store(Context ctx, String key, Integer val) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(database, MODE_PRIVATE);
        sharedPreferences.edit().putInt(key, val).apply();
    }

    public static String restoreS(Context ctx, String key) {
        SharedPreferences sh = ctx.getSharedPreferences(database, MODE_PRIVATE);
        return sh.getString(key, "");
    }

    public static String restoreS(Context ctx, String key, String def) {
        SharedPreferences sh = ctx.getSharedPreferences(database, MODE_PRIVATE);
        return sh.getString(key, def);
    }

    public static Integer restoreI(Context ctx, String key) {
        SharedPreferences sh = ctx.getSharedPreferences(database, MODE_PRIVATE);
        return sh.getInt(key, 0);
    }
}
