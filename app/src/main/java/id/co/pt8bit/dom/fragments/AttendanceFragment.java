package id.co.pt8bit.dom.fragments;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import java.text.DateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.pt8bit.dom.R;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AttendanceFragment#newInstance} factory method to
 * create an instance of getContext() fragment.
 */
@RuntimePermissions
public class AttendanceFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.history)
    LinearLayoutCompat history;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String cincout;
    private int iconcincout;

    private int REQUEST_IMAGE_CAPTURE = 101;

    public AttendanceFragment() {
        // Required empty public constructor
    }

    /**
     * Use getContext() factory method to create a new instance of
     * getContext() fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AttendanceFragment newInstance(String param1, String param2) {
        AttendanceFragment fragment = new AttendanceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_attendance, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        TextView textView = view.findViewById(R.id.text);
//        textView.setText(mParam1);
//        textView.setOnClickListener(v -> Toast.makeText(v.getContext(), mParam2, Toast.LENGTH_SHORT).show());
    }

    @OnClick({R.id.checkin, R.id.checkout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.checkin:
                AttendanceFragmentPermissionsDispatcher.showCameraWithPermissionCheck(this);
                cincout = "Absen Masuk";
                iconcincout = R.drawable.ic_login;
                break;
            case R.id.checkout:
                AttendanceFragmentPermissionsDispatcher.showCameraWithPermissionCheck(this);
                cincout = "Absen Keluar";
                iconcincout = R.drawable.ic_logout;
                break;
        }
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    void showCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @OnShowRationale(Manifest.permission.CAMERA)
    void showRationaleForCamera(final PermissionRequest request) {
        new AlertDialog.Builder(getContext())
                .setMessage("Rationale")
                .setPositiveButton(android.R.string.ok, (dialog, button) -> request.proceed())
                .setNegativeButton(android.R.string.cancel, (dialog, button) -> request.cancel())
                .show();
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    void showDeniedForCamera() {
        Toast.makeText(getContext(), "denied", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void showNeverAskForCamera() {
        Toast.makeText(getContext(), "never ask", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        AttendanceFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bmp = (Bitmap) extras.get("data");
            history(bmp);
        }
    }

    private void history(Bitmap bmp) {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.item_history_attendance, null);
        AppCompatImageView image = v.findViewById(R.id.image);
        AppCompatTextView inout = v.findViewById(R.id.inout);
        AppCompatTextView time = v.findViewById(R.id.time);
        AppCompatImageView icon = v.findViewById(R.id.icon);

        image.setImageBitmap(bmp);
        inout.setText(cincout);
        time.setText(DateFormat.getDateTimeInstance().format(new Date()));
        icon.setImageResource(iconcincout);

        history.addView(v, 0);
    }
}