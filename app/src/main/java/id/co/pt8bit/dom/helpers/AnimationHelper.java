package id.co.pt8bit.dom.helpers;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import id.co.pt8bit.dom.R;

public class AnimationHelper {

    public static void fadeIn(View v, int seconds) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.fadein);
        as.setDuration(seconds * 1000);
        v.startAnimation(as);
    }

    public static void fadeOut(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.fadeout);
        as.setDuration(d);
        v.startAnimation(as);
    }

    public static void slidedown(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.slidedown);
        as.setDuration(d);
        v.startAnimation(as);
    }

    public static void slideup(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.slideup);
        as.setDuration(d);
        v.startAnimation(as);
    }

    public static void shake(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.shake);
        as.setDuration(d);
        v.startAnimation(as);
    }

    public static void popuphide(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.popup_hide);
        as.setDuration(d);
        v.startAnimation(as);
    }

    public static void popupshow(View v, int d) {
        Context ctx = v.getContext();
        Animation as = AnimationUtils.loadAnimation(ctx, R.anim.popup_show);
        as.setDuration(d);
        v.startAnimation(as);
    }

}
