package id.co.pt8bit.dom.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.pt8bit.dom.R;
import id.co.pt8bit.dom.helpers.CookieHelper;

public class WizardActivity extends BaseActivity {

    private static final int MAX_STEP = 3;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.layoutDots)
    LinearLayoutCompat layoutDots;
    @BindView(R.id.btn_next)
    AppCompatTextView btnNext;
    @BindView(R.id.icon)
    LottieAnimationView icon;
    @BindView(R.id.layout_button)
    LinearLayoutCompat layoutButton;

    private String[] about_title_array = {
            "Task Scedule & Manager",
            "Attendance & Map Tracking",
            "Human Resource Managament"
    };
    private String[] about_description_array = {
            "The assignment of start and end times to a set of tasks, subject to certain constraints.",
            "The action or state of going regularly to or being present at a place or event.",
            "Recruiting, selecting, inducting employees, providing orientation, imparting training and development."
    };
    private int[] about_images_array = {
            R.drawable.step1,
            R.drawable.step2,
            R.drawable.step3
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        // adding bottom dots
        bottomProgressDots(0);

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        btnNext.setOnClickListener(v -> {
            int current = viewPager.getCurrentItem() + 1;
            if (current < MAX_STEP) {
                // move to next screen
                viewPager.setCurrentItem(current);
            } else {
                CookieHelper.store(this, "wizard", 0);
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        });
    }

    private void bottomProgressDots(int current_index) {
        AppCompatImageView[] dots = new AppCompatImageView[MAX_STEP];

        layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new AppCompatImageView(this);
            int width_height = 15;
            LinearLayoutCompat.LayoutParams params = new LinearLayoutCompat.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.wizard_dot);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            layoutDots.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.wizard_dot);
            dots[current_index].setColorFilter(getResources().getColor(R.color.orange_400), PorterDuff.Mode.SRC_IN);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(final int position) {
            bottomProgressDots(position);
            if (position == about_title_array.length - 1) {
                btnNext.setText(getString(R.string.start));
                layoutButton.setBackgroundColor(getResources().getColor(R.color.orange_400));
                btnNext.setTextColor(Color.WHITE);
                icon.setAnimation("check.json");
            } else {
                btnNext.setText(getString(R.string.next));
                layoutButton.setBackgroundColor(getResources().getColor(R.color.grey_10));
                btnNext.setTextColor(getResources().getColor(R.color.grey_90));
                icon.setAnimation("right.json");
            }
            icon.playAnimation();
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {

        @BindView(R.id.image)
        AppCompatImageView image;
        @BindView(R.id.title)
        AppCompatTextView title;
        @BindView(R.id.description)
        AppCompatTextView description;

        public MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_stepper_wizard, container, false);
            ButterKnife.bind(this, view);
            title.setText(about_title_array[position]);
            description.setText(about_description_array[position]);
            image.setImageResource(about_images_array[position]);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return about_title_array.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}