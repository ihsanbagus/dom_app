package id.co.pt8bit.dom.helpers;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import static android.content.Context.CLIPBOARD_SERVICE;

public class StringHelper {

    public static boolean duplicateChar(String word, String find) {
        String temp = word.replace(find, "");
        int occ = (word.length() - temp.length()) / find.length();
        return occ > 1;
    }

    public static String splitDotNumber(String s) {
        String m;
        try {
            m = s.split("\\.")[1];
        } catch (Exception e) {
            m = s;
        }
        return m;
    }

    public static void copyText(Context ctx, String text) {
        ClipboardManager clipboard = (ClipboardManager) ctx.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("sn", text);
        clipboard.setPrimaryClip(clip);
    }

}
