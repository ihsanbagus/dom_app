package id.co.pt8bit.dom.materials;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import id.co.pt8bit.dom.helpers.SizeHelper;

public class CustomButton extends AppCompatButton {

    public CustomButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTextColor(getResources().getColor(android.R.color.white));
        setTypeface(getTypeface(), Typeface.BOLD);
        setPadding(size(10), 0, size(10), size(5));
    }

    private int size(int num) {
        return SizeHelper.intToDP(this.getContext(), num);
    }

//    @Override
//    public void setPressed(boolean pressed) {
//        if (pressed) {
//            setPadding(size(10), size(4), size(10), 0);
//        } else {
//            setPadding(size(10), 0, size(10), size(4));
//        }
//        super.setPressed(pressed);
//    }
}