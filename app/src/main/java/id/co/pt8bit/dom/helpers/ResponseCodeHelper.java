package id.co.pt8bit.dom.helpers;

import java.util.HashMap;
import java.util.Map;

public class ResponseCodeHelper {
    public static Map<String, String> get(String rc) {
        Map<String, String> hasil = new HashMap<>();
        switch (rc) {
            case "00":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi Sukses");
                hasil.put("status", "Sukses");
                break;
            case "01":
                hasil.put("code", rc);
                hasil.put("message", "Timeout");
                hasil.put("status", "Gagal");
                break;
            case "02":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi Gagal");
                hasil.put("status", "Gagal");
                break;
            case "03":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi Pending");
                hasil.put("status", "Pending");
                break;
            case "40":
                hasil.put("code", rc);
                hasil.put("message", "Payload Error");
                hasil.put("status", "Gagal");
                break;
            case "41":
                hasil.put("code", rc);
                hasil.put("message", "Signature tidak valid");
                hasil.put("status", "Gagal");
                break;
            case "42":
                hasil.put("code", rc);
                hasil.put("message", "Gagal memproses API Buyer");
                hasil.put("status", "Gagal");
                break;
            case "43":
                hasil.put("code", rc);
                hasil.put("message", "SKU tidak di temukan atau Non-Aktif");
                hasil.put("status", "Gagal");
                break;
            case "44":
                hasil.put("code", rc);
                hasil.put("message", "Saldo tidak cukup");
                hasil.put("status", "Gagal");
                break;
            case "45":
                hasil.put("code", rc);
                hasil.put("message", "IP Anda tidak kami kenali");
                hasil.put("status", "Gagal");
                break;
            case "46":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi sudah pernah terjadi sebelumnya");
                hasil.put("status", "Gagal");
                break;
            case "47":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi sudah terjadi di buyer lain");
                hasil.put("status", "Gagal");
                break;
            case "49":
                hasil.put("code", rc);
                hasil.put("message", "Ref ID tidak unik");
                hasil.put("status", "Gagal");
                break;
            case "50":
                hasil.put("code", rc);
                hasil.put("message", "Transaksi Tidak Ditemukan");
                hasil.put("status", "Gagal");
                break;
            case "51":
                hasil.put("code", rc);
                hasil.put("message", "Nomor Tujuan Diblokir");
                hasil.put("status", "Gagal");
                break;
            case "52":
                hasil.put("code", rc);
                hasil.put("message", "Prefix Tidak Sesuai Dengan Operator");
                hasil.put("status", "Gagal");
                break;
            case "53":
                hasil.put("code", rc);
                hasil.put("message", "Produk Seller Sedang Tidak Tersedia");
                hasil.put("status", "Gagal");
                break;
            case "54":
                hasil.put("code", rc);
                hasil.put("message", "Nomor Tujuan Salah");
                hasil.put("status", "Gagal");
                break;
            case "55":
                hasil.put("code", rc);
                hasil.put("message", "Produk Sedang Gangguan");
                hasil.put("status", "Gagal");
                break;
            case "56":
                hasil.put("code", rc);
                hasil.put("message", "Saldo Seller Digiflazz Habis");
                hasil.put("status", "Gagal");
                break;
            case "57":
                hasil.put("code", rc);
                hasil.put("message", "Jumlah Digit Kurang Atau Lebih");
                hasil.put("status", "Gagal");
                break;
            case "58":
                hasil.put("code", rc);
                hasil.put("message", "SSedang Cut Off");
                hasil.put("status", "Gagal");
                break;
            case "59":
                hasil.put("code", rc);
                hasil.put("message", "Tujuan di Luar Wilayah/Cluster");
                hasil.put("status", "Gagal");
                break;
            case "60":
                hasil.put("code", rc);
                hasil.put("message", "Tagihan belum tersedia");
                hasil.put("status", "Gagal");
                break;
            case "61":
                hasil.put("code", rc);
                hasil.put("message", "Belum pernah melakukan deposit");
                hasil.put("status", "Gagal");
                break;
            case "62":
                hasil.put("code", rc);
                hasil.put("message", "Seller sedang mengalami gangguan");
                hasil.put("status", "Gagal");
                break;
            case "63":
                hasil.put("code", rc);
                hasil.put("message", "Tidak support transaksi multi");
                hasil.put("status", "Gagal");
                break;
            case "64":
                hasil.put("code", rc);
                hasil.put("message", "Tarik tiket gagal, coba nominal lain atau hubungi admin.");
                hasil.put("status", "Gagal");
                break;
            case "65":
                hasil.put("code", rc);
                hasil.put("message", "Limit transaksi multi");
                hasil.put("status", "Gagal");
                break;
            case "66":
                hasil.put("code", rc);
                hasil.put("message", "Cut Off (Perbaikan Sistem Seller)");
                hasil.put("status", "Gagal");
                break;
            case "67":
                hasil.put("code", rc);
                hasil.put("message", "Seller belum ter-verfikasi");
                hasil.put("status", "Gagal");
                break;
            case "68":
                hasil.put("code", rc);
                hasil.put("message", "Stok habis");
                hasil.put("status", "Gagal");
                break;
            case "69":
                hasil.put("code", rc);
                hasil.put("message", "Harga seller lebih besar dari ketentuan harga Buyer");
                hasil.put("status", "Gagal");
                break;
            case "70":
                hasil.put("code", rc);
                hasil.put("message", "Timeout Dari Biller");
                hasil.put("status", "Gagal");
                break;
            case "99":
                hasil.put("code", rc);
                hasil.put("message", "DF Router Issue");
                hasil.put("status", "Pending");
                break;
            default:
                hasil.put("code", rc);
                hasil.put("message", "Kode Tidak Ditemukan");
                hasil.put("status", "Gagal");
                break;
        }
        return hasil;
    }
}
