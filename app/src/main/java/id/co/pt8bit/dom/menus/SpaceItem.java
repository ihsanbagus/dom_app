package id.co.pt8bit.dom.menus;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import id.co.pt8bit.dom.helpers.SizeHelper;

public class SpaceItem extends DrawerItem<SpaceItem.ViewHolder> {

    private int spaceDp;

    public SpaceItem(int spaceDp) {
        this.spaceDp = spaceDp;
    }

    @Override
    public ViewHolder createViewHolder(ViewGroup parent) {
        Context c = parent.getContext();
        View view = new View(c);
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                SizeHelper.intToDP(c, spaceDp)));
        return new ViewHolder(view);
    }

    @Override
    public void bindViewHolder(ViewHolder holder) {
    }

    @Override
    public boolean isSelectable() {
        return false;
    }

    static class ViewHolder extends DrawerAdapter.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}